<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    $data = App\Product::all()->toArray();
    return view('home', ['data' => $data]);
});

Route::get('/page/{slug}', function ($slug) {
    $data = App\Page::where('slug', $slug)->firstOrFail()->toArray();
    return view('page', ['data' => $data]);
});

Route::get('/product/{id}', function ($id) {
    $data = App\Product::where('id', $id)->firstOrFail()->toArray();
    return view('product', ['data' => $data]);
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
