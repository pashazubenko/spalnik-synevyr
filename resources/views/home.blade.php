@extends('layouts.index')
@section('content')
  <div class="row">
  @foreach($data as $product)
    <div class="col-12 col-sm-4">
        <div class="card" style="background-color: #f5f5f5; border-color: #e2e2e2;">
          <a href="/product/{{ $product['id'] }}">
            <img class="card-img-top img-fluid" src="/storage/{{ $product['image'] }}" alt="Card image">
          </a>
          <div class="card-block">
            <h3 class="card-title">{{ $product['title'] }}</h3>
            <p class="card-text">{{ $product['excerpt'] }}</p>
            <a href="/product/{{ $product['id'] }}" class="button fit buy ">{{ $product['prise'] }} ₴</a>
          </div>
        </div>
      </div>
  @endforeach
  </div>
@endsection
