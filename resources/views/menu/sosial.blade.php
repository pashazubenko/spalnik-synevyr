
<ul class="icons">
    @foreach($items as $menu_item)
        <li>
        	<a href="{{ $menu_item->url }}" class="icon {{ $menu_item->icon_class }}">
        		<span class="label">{{ $menu_item->title }}</span>
        	</a>
        </li>
    @endforeach
</ul>
