
<ul class="links">
    @foreach($items as $menu_item)
        <li class="{{ Request::is($menu_item->url) ? 'active' : '' }}">
        	<a href="{{ $menu_item->url }}" class="{{ $menu_item->icon_class }}">
        		{{ $menu_item->title }}
        	</a>
        </li>
    @endforeach
</ul>
