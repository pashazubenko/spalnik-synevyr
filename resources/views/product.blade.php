@extends('layouts.index') @section('content')
<header class="major">
    <h1>{{ $data['title'] }}</h1>
</header>
<div class="row">
    <div class="6u 12u$(xsmall)">
        <span class="image fit">
          <img src="/storage/{{ $data['image'] }}" alt="">
        </span>
    </div>
    <div class="6u$ 12u$(xsmall)">
        <p>{{ $data['excerpt'] }}</p>
        <h3>
          {{ $data['prise'] }} ₴
        </h3>
        <form  class="alt">
            <div class="row">
                <div class="2u 12u$(xsmall)">
                    <div class="select-wrapper">
                        <select name="demo-category" id="demo-category">
                            <option value="1">1 шт.</option>
                            <option value="2">2 шт.</option>
                            <option value="3">3 шт.</option>
                            <option value="4">4 шт.</option>
                            <option value="5">5 шт.</option>
                        </select>
                    </div>
                </div>
                <div class="5u 12u$(xsmall)">
                    <input type="text" name="demo-name" id="demo-name" value="" placeholder="Ім'я">
                </div>
                <div class="5u$ 12u$(xsmall)">
                    <input type="email" name="demo-email" id="demo-email" value="" placeholder="Телефон">
                </div>
            </div>
            <div class="row">
                <div class="12u$">
                    <ul class="actions">
                        <li>
                            <input type="submit" value="Замовити" class="special">
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">

  @if($data['image_gallery'])
    @php
      $image_gallery = $data['image_gallery'];
      $image_gallery = str_replace(array("'",'"'), '', $image_gallery);
      $image_gallery = str_replace(array('[',']'), '', $image_gallery);
      $image_gallery = str_replace('\\', '', $image_gallery);
      $image_gallery = explode(',', $image_gallery);
    @endphp
    @foreach($image_gallery as $image)
      <div class="4u$"><span class="image fit"><img src="/storage/{{{ $image }}}" alt=""></span></div>
    @endforeach
  @endif

  <div class="12u$">
    <header>
      <h2>Опис</h2>
    </header>
    {!! $data['body'] !!}
  </div>
</div>

@endsection
