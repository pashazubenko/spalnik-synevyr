@extends('layouts.index')
@section('content')
  <section class="post">
    <header class="major">
      <h1>{{ $data['title'] }}</h1>
    </header>
    <!-- <div class="image main"><img src="/storage/{{ $data['image'] }}" alt=""></div> -->
    {!! $data['body'] !!}
  </section>
@endsection
