<!DOCTYPE HTML>
<html>
  <head>
    <title>{{ Voyager::setting('site.title') }}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/local.css') }}" />
    <noscript><link rel="stylesheet" href="{{ asset('css/noscript.css') }}" /></noscript>
  </head>
  <body class="is-loading">

    <!-- Wrapper -->
      <div id="wrapper" class="fade-in">

          <!-- Intro -->
          <div id="intro">
            <h1 class="site-title">{!! Voyager::setting('site.title') !!}</h1>
            <p>{!! Voyager::setting('site.description') !!}</p>
            <ul class="actions">
              <li><a href="#header" class="button icon solo fa-arrow-down scrolly">Continue</a></li>
            </ul>
          </div>

          <!-- Header -->
          <header id="header">
            <a class="logo site-title">Синевир</a>
          </header>

          <!-- Nav -->
          <nav id="nav">
            {{ menu('main', 'menu.main') }}
          </nav>

        <!-- Main -->
        <div id="main" class="container">
          @yield('content')
        </div>

        <!-- Footer -->
          <footer id="footer">
            <section>
              <section>
                <h3>Соцiальнi мережi</h3>
                {{ menu('sosial', 'menu.sosial') }}
              </section>
            </section>
            <section class="split contact">
              <section class="alt">
                <h3>Адресса</h3>
                <p>{!! Voyager::setting('site.adress') !!}</p>
              </section>
              <section>
                <h3>Email</h3>
                <p><a href="mailto:{!! Voyager::setting('site.email') !!}">{!! Voyager::setting('site.email') !!}</a></p>
              </section>
            </section>
          </footer>

          <!-- Copyright -->
          <div id="copyright">
            <ul><li>&copy; Untitled</li><li></li></ul>
          </div>
      </div>

      <!-- Scripts -->
      <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
      <script src="{{ asset('js/jquery.scrolly.min.js') }}"></script>
      <script src="{{ asset('js/skel.min.js') }}"></script>
      <script src="{{ asset('js/util.js') }}"></script>
      <script src="{{ asset('js/main.js') }}"></script>

  </body>
</html>
